# coinflip-5-3-calculator

Implements a few different ways of counting the odds in a coin flip game.  In the game, you flip a coin repeatedly.  If you get heads 5 times before you get tails 3 times, you win, otherwise you lose.

The odds are very simple. The win to loss odds are 3:5, for a ratio of 3/8, or 37.5% chance of winning.  The main reason this is being uploaded is for the iterator solution, which is somewhat similar to a lexicographic permutations iterator.
