#[derive(Clone, Copy, Default, Debug, Hash, PartialEq, Eq, PartialOrd, Ord)]
pub struct Running {
    heads: u8,
    tails: u8,
}

#[derive(Clone, Copy, Debug, Hash, PartialEq, Eq, PartialOrd, Ord)]
pub enum HeadsFlipResult {
    Running(Running),
    Win,
}
#[derive(Clone, Copy, Debug, Hash, PartialEq, Eq, PartialOrd, Ord)]
pub enum TailsFlipResult {
    Running(Running),
    Loss,
}

impl Running {
    pub fn flip_heads(self) -> HeadsFlipResult {
        if self.heads == 4 {
            HeadsFlipResult::Win
        } else {
            HeadsFlipResult::Running(Running {
                heads: self.heads + 1,
                tails: self.tails,
            })
        }
    }

    pub fn flip_tails(self) -> TailsFlipResult {
        if self.tails == 2 {
            TailsFlipResult::Loss
        } else {
            TailsFlipResult::Running(Running {
                tails: self.tails + 1,
                heads: self.heads,
            })
        }
    }
}

#[derive(Clone, Copy, Default, Debug, Hash, PartialEq, Eq, PartialOrd, Ord)]
pub struct Tracker {
    pub wins: u64,
    pub losses: u64,
}

/// Just iteratively run through all wins and losses in order, keeping track of them in the tracker.
pub fn run_possibilities(running: Running, tracker: &mut Tracker) {
    match running.flip_heads() {
        HeadsFlipResult::Running(running) => run_possibilities(running, tracker),
        HeadsFlipResult::Win => tracker.wins += 1,
    }

    match running.flip_tails() {
        TailsFlipResult::Running(running) => run_possibilities(running, tracker),
        TailsFlipResult::Loss => tracker.losses += 1,
    }
}

#[derive(Clone, Copy, Debug, Hash, PartialEq, Eq, PartialOrd, Ord)]
pub enum Flip {
    Heads,
    Tails,
}

#[derive(Clone, Copy, Debug, Hash, PartialEq, Eq, PartialOrd, Ord)]
pub enum Outcome {
    Win,
    Loss,
}

/// An iterator over win and loss conditions.
///
/// Follows a simple algorithm:
///
/// * If the flips are all tails, pop them all and return None (allowing the iterator to cycle).
/// * Pop all tails results off the end.
/// * If there are any heads, replace the rightmost one with a tails.
/// * Add heads flips until the current check is done.
/// * If the rightmost result is heads, return a win, otherwise return a loss.
#[derive(Clone, Copy, Default, Debug, Hash, PartialEq, Eq, PartialOrd, Ord)]
pub struct WinLossIterator {
    flips: [Option<Flip>; 7],
}

impl Iterator for WinLossIterator {
    type Item = Outcome;

    fn next(&mut self) -> Option<Self::Item> {
        use Flip::*;
        use Outcome::*;

        if self.flips
            == [
                Some(Tails),
                Some(Tails),
                Some(Tails),
                None,
                None,
                None,
                None,
            ]
        {
            self.flips = [None, None, None, None, None, None, None];
            return None;
        }

        // Pop all tails results off end.
        for i in self
            .flips
            .iter_mut()
            .rev()
            .skip_while(|i| i.is_none())
            .take_while(|i| **i == Some(Tails))
        {
            *i = None;
        }

        if let Some(head) = self.flips.iter_mut().rev().find(|i| **i == Some(Heads)) {
            *head = Some(Tails);
        }

        let tails_count = self.flips.iter().filter(|i| **i == Some(Tails)).count();
        if tails_count == 3 {
            Some(Loss)
        } else {
            let heads_count = self.flips.iter().filter(|i| **i == Some(Heads)).count();

            // Add heads until the count is right
            for i in self
                .flips
                .iter_mut()
                .filter(|i| i.is_none())
                .take(5 - heads_count)
            {
                *i = Some(Heads);
            }

            Some(Win)
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_run_possibilities() {
        let mut tracker = Tracker::default();
        run_possibilities(Default::default(), &mut tracker);
        assert_eq!(
            tracker,
            Tracker {
                wins: 21,
                losses: 35
            }
        );
    }

    #[test]
    fn test_iterator() {
        let mut tracker = Tracker::default();
        for outcome in WinLossIterator::default() {
            match outcome {
                Outcome::Win => tracker.wins += 1,
                Outcome::Loss => tracker.losses += 1,
            }
        }
        assert_eq!(
            tracker,
            Tracker {
                wins: 21,
                losses: 35
            }
        );
    }
}
